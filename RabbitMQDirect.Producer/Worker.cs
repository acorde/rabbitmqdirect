using System.Text.Json;
using System.Text;
using RabbitMQ.Client;
using RabbitMQDirect.Producer.Models;
using System.Reflection.Emit;

namespace RabbitMQDirect.Producer
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (_logger.IsEnabled(LogLevel.Information))
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                    Producer();
                }
                await Task.Delay(1000, stoppingToken);
            }
        }

        private static void Producer()
        {
            var factory = new ConnectionFactory() { HostName = "localhost"};
            var manualResentEvent = new ManualResetEvent(false);
            manualResentEvent.Reset();
            using (var connection = factory.CreateConnection())
            {
                var queueName = "order";
                var channel1 = SetupChannel(connection);
                BuildAndRunPublishers(channel1, queueName, "Produtor A", manualResentEvent);
                manualResentEvent.WaitOne();
            }            
        }

        private static IModel SetupChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            channel.QueueDeclare(queue: "queue-order", durable: false, exclusive: false, autoDelete: false, arguments: null);
            channel.QueueDeclare(queue: "queue-finance", durable: false, exclusive: false, autoDelete: false, arguments: null);

            channel.ExchangeDeclare("exchange-order", ExchangeType.Direct);

            channel.QueueBind("queue-order", exchange: "exchange-order", routingKey: "routing-new");
            channel.QueueBind("queue-order", exchange: "exchange-order", routingKey: "routing-upd");
            channel.QueueBind("queue-finance", exchange: "exchange-order", routingKey: "routing-new");

            return channel;

        }

        private static void BuildAndRunPublishers(IModel channel, string queue, string publishName, ManualResetEvent manualResetEvent)
        {
            Task.Run(() =>
            {
            var IdIndex = 1;
            var randon = new Random(DateTime.UtcNow.Microsecond * DateTime.UtcNow.Second);

                while (true)
                {
                    try
                    {
                        Console.WriteLine("Pressione qualquer tecla para produzir mensagens!");
                        Console.ReadLine();

                        var order = new Orders(IdIndex++, randon.Next(1000, 9999));
                        var message1 = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(order));
                        channel.BasicPublish(exchange: "exchange-order", routingKey: "routing-new", basicProperties: null, body: message1);
                        Console.WriteLine($"New order Id: {order.Id}: Amount: {order.Amount} | Created: {order.CreateDate:o}");

                        order.UpdateOrder(randon.Next(100, 200));
                        var message2 = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(order));
                        channel.BasicPublish(exchange: "exchange-order", routingKey: "routing-upd", basicProperties: null, body: message2);
                        Console.WriteLine($"Upd Id: {order.Id}: Amount: {order.Amount} | Created: {order.CreateDate:o}");

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
            });
        }
    }
}
