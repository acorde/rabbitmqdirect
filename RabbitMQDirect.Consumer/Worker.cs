using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQDirect.Producer.Models;
using System.Text;

namespace RabbitMQDirect.Consumer
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (_logger.IsEnabled(LogLevel.Information))
                {
                    _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                    Consumer();
                }
                await Task.Delay(1000, stoppingToken);
            }
        }

        private static void Consumer()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                var channel = connection.CreateModel();
                var queueName = "queue-order";

                channel.QueueDeclare(queue: queueName,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
                BuildAndRunWorker(channel, $"Worker A1", "queue-order");
                BuildAndRunWorker(channel, $"Worker A2", "queue-order");
                BuildAndRunWorker(channel, $"Worker A3", "queue-order");
                BuildAndRunWorker(channel, $"Worker B4", "queue-finance");
                BuildAndRunWorker(channel, $"Worker B5", "queue-finance");
                BuildAndRunWorker(channel, $"Worker B6", "queue-finance");

                Console.ReadLine();
            };
        }

        private static void BuildAndRunWorker(IModel channel, string workerName, string queueName)
        {
            channel.BasicQos(prefetchSize: 0, prefetchCount: 7, global: false);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                try
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body.ToArray());
                    var order = System.Text.Json.JsonSerializer.Deserialize<Orders>(message);
                    Console.WriteLine(
                                $"{channel.ChannelNumber} - {queueName}- {workerName}: [x] Order Id {order.Id} | {order.Amount} | {order.CreateDate:s} | {order.LastUpdated:s}");

                    channel.BasicAck(ea.DeliveryTag, false);
                    Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    channel.BasicNack(ea.DeliveryTag, false, true);
                }
            };
            channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);
        }
    }
}
